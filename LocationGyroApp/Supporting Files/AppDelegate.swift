import UIKit
import CoreLocation

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        let videoURL = URL(
            string: "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/WeAreGoingOnBullrun.mp4"
        )
        let motionService = MotionService()
        let location = LocationService(
            coordinates: CLLocation(latitude: 49.992725, longitude: 30.330984)
        )
        guard let url = videoURL else { return false }

        window = UIWindow()
        window?.rootViewController = VideoScreen(videoURL: url,
                                                 location: location,
                                                 motionService: motionService)
        window?.makeKeyAndVisible()

        return true
    }

}
