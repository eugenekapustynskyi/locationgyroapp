import UIKit
import AVKit

final class VideoScreen: UIViewController {

    //MARK: - Private Variables

    private let videoPlayer: AVPlayer
    private var location: LocationServiceProtocol
    private var motionService: MotionServiceProtocol

    //MARK: - Initialisers

    init(videoURL: URL,
         location: LocationServiceProtocol,
         motionService: MotionServiceProtocol) {
        self.videoPlayer = AVPlayer(url: videoURL)
        self.motionService = motionService
        self.location = location

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //MARK: - LifeCycle

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        setupView()
        setupLocationService()
        setupGyro()
    }

    //MARK: - Setup View

    private func setupView() {
        setupPlayer()
    }

}

//MARK: - Setup Player

private extension VideoScreen {

    private func setupPlayer() {
        let vc = AVPlayerViewController()
        vc.player = videoPlayer
        present(vc, animated: true)

        videoPlayer.play()
    }

    private func currentTimeControl(_ value: Double) {
        var timeProgress = CMTime()
        guard let currentMoment = videoPlayer.currentItem?.currentTime().seconds
        else { return }

        if value > 1 {
            timeProgress = CMTime(seconds: currentMoment + value, preferredTimescale: 1)
            videoPlayer.seek(to: timeProgress)
        }
        if value < -1 {
            timeProgress = CMTime(seconds: currentMoment - value, preferredTimescale: 1)
            videoPlayer.seek(to: timeProgress)
        }
    }

    private func soundControl(_ xValue: Double) {
        if xValue > 1 {
            videoPlayer.volume += 0.1
        }
        if xValue < -1 {
            videoPlayer.volume -= 0.1
        }
    }

}

//MARK: - Motion Handling

extension VideoScreen {

    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            videoPlayer.pause()
        }
    }

     func setupGyro() {
        motionService.gyroHandler = { [weak self] (xValue, zValue) in
            self?.currentTimeControl(zValue)
            self?.soundControl(xValue)
        }
    }

}

//MARK: - Location Service

private extension VideoScreen {

    func setupLocationService() {
        location.coordsHandler = { [weak self] distance in
            self?.videoPlayer.seek(to: .zero)
        }
    }

}
