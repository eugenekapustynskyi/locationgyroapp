import CoreMotion

protocol MotionServiceProtocol {
    var gyroHandler: (Double, Double) -> Void { get set }
    func getGyroUpdates()
}

struct MotionService {

    var gyroHandler: (Double, Double) -> Void = {_,_ in }

    //MARK: - Private Variables

    private let motionManager = CMMotionManager()

    //MARK: - Initialisers

    init() {
        motionManager.startGyroUpdates()
        getGyroUpdates()
    }

}

//MARK: - MotionServiceProtocol

extension MotionService: MotionServiceProtocol {

    func getGyroUpdates() {
        let timer = Timer(timeInterval: 1, repeats: true, block: {_ in
            guard let data = self.motionManager.gyroData
            else { return }

            let zValue = data.rotationRate.z
            let xValue = data.rotationRate.x

            self.gyroHandler(xValue, zValue)
        })
        RunLoop.current.add(timer, forMode: .common)
    }

}
