import CoreLocation
import UIKit

protocol LocationServiceProtocol {
    var coordsHandler: ((CLLocationDistance) -> Void)? { get set }
}

final class LocationService: NSObject, LocationServiceProtocol {

    var coordsHandler: ((CLLocationDistance) -> Void)?

    //MARK: - Private Variables

    private var initialCoordinates: CLLocation
    private var manager: CLLocationManager!

    //MARK: - Initialisers

    init(coordinates: CLLocation) {
        self.initialCoordinates = coordinates

        super.init()
        self.setupUserLocation()
    }

    private func setupUserLocation() {
        manager = CLLocationManager()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
    }

}

//MARK: - CLLocationManagerDelegate

extension LocationService: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }

        let distance = initialCoordinates.distance(from: location)
        if distance > 10 {
            coordsHandler?(distance)
            initialCoordinates = location
        }
    }

}
